import 'package:awesome_dropdown/awesome_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/svg.dart';

class MainTemplate extends StatefulWidget {
  MainTemplate({Key? key}) : super(key: key);

  @override
  _MainTemplateState createState() => _MainTemplateState();
}

class _MainTemplateState extends State<MainTemplate> {
  List<String> pics = [
    'micro.png',
    'vapor.png',
    'meat.png',
    'weight.png',
    'bed.png',
    'wine.png'
  ];
  List<String> price = [
    '165 300 тг.',
    '199 990 тг.',
    '42 990 тг.',
    '34 200 тг.',
    '\$1165',
    '151 200 тг.'
  ];
  List<String> name = [
    'Микроволновая печь Bork W702',
    'Парогенератор с утюгом Braun Carestyle 7 pro',
    'Мясорубка Moulinex ME108832',
    'Напольные весы Bork N500 gg',
    'Комплект постельного белья Frette',
    'Набор из двух бокалов для вина Baccarat (Артикул: 2100293)',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Text(
                    'RSVP',
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Text(
                    'Wishlist',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'RU',
                    style: TextStyle(color: Colors.black),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  SvgPicture.asset('assets/downArrow.svg'),
                ],
              ),
            ],
          ),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 400.0),
                child: Image.asset('mainLogo.png'),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 20,
            ),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: Image.asset('tayni.png'),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 40,
            ),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.black)),
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Image.asset('forest.png'),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 60,
            ),
          ),
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    SvgPicture.asset('assets/date.svg'),
                    SizedBox(height: 40),
                    SvgPicture.asset('assets/covid.svg'),
                    SizedBox(height: 40),
                    SvgPicture.asset('assets/rule.svg'),
                  ],
                ),
                SizedBox(width: 20),
                Column(
                  children: [
                    SvgPicture.asset('assets/place.svg'),
                    SizedBox(height: 40),
                    Stack(
                      children: [
                        SvgPicture.asset('assets/dresscode.svg'),
                        Positioned(
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Image.asset('wedd.png'),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 120,
            ),
          ),
          SliverToBoxAdapter(child: SvgPicture.asset('assets/form.svg')),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 95,
            ),
          ),
          SliverToBoxAdapter(child: SvgPicture.asset('assets/presentsH.svg')),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 40,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/present1.svg'),
                      SizedBox(
                        width: 22,
                      ),
                      SvgPicture.asset('assets/present2.svg'),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  SvgPicture.asset('assets/present3.svg'),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: 40)),
          SliverToBoxAdapter(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Stack(
                      children: [
                        SvgPicture.asset('assets/microwave.svg'),
                        Image.asset('micro.png'),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Stack(
                      children: [
                        SvgPicture.asset('assets/weight.svg'),
                        Image.asset('weight.png'),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Stack(
                      children: [
                        SvgPicture.asset('assets/vapor.svg'),
                        Image.asset('vapor.png'),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Stack(
                      children: [
                        SvgPicture.asset('assets/bed.svg'),
                        Image.asset('bed.png'),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Stack(
                      children: [
                        SvgPicture.asset('assets/meat.svg'),
                        Image.asset('meat.png'),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Stack(
                      children: [
                        SvgPicture.asset('assets/wine.svg'),
                        Image.asset('wine.png'),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          SliverFillRemaining(),
        ],
      ),
    );
  }
}
